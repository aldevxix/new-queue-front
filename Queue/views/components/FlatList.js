import React, { Component } from 'react';
import { GlobalStyles } from '../../assets/GlobalStyles';
import { View, FlatList, Text,TouchableOpacity, Alert,Image, RefreshControl ,Dimensions, ImageBackground} from 'react-native';
import CardView from 'react-native-cardview';
import { AppConstants } from "../../systems/Constants";
import ImageView from '../components/ImageView';

import {link} from '../../systems/Config'

export default class FlatListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            
        };
    }

    FlatListItemSeparator = () => {
        return (
          <View style={{ height: 2, width: "100%", backgroundColor: "#EDEDED" }} />
        );
    };

    GetItem(item) {
        this.props.onPress(item);
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        fetchData().then(() => {
          this.setState({refreshing: false});
        });
      }
      

    render() {
        return (
            <View>
                <FlatList
                    data={this.props.data}
                    sisaAntrean={this.props.sisaAntrean}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                        <TouchableOpacity activeOpacity={0.8}
                            onPress={() => this.props.onItemClick(
                                {
                                    picture: link + item.picture,
                                    alamat: item.address,
                                    name: item.name_place,
                                    id: item.id_place,
                                    status: item.status
                                }
                            )}>
                            <View style={{ alignItems: 'center' }}>
                                <CardView
                                    style={{ marginTop: 8 }}
                                    width={Dimensions.get('window').width - (4 * AppConstants.ActiveTheme.AppObjectSpacing)}
                                    height={Dimensions.get('window').height / 2.2}
                                    cardElevation={2}
                                    cardMaxElevation={1}
                                    cornerRadius={5}>
                                    <Image
                                        style={{
                                            alignItems: 'center',
                                            width: Dimensions.get('window').width / 1 - (30),
                                            height: Dimensions.get('window').height / 3,
                                        }}
                                        radius={AppConstants.ActiveTheme.AppObjectSpacing}
                                        source={{ uri: link + item.picture }} >
                                    </Image>
                                    <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                        <View style={{
                                            width: '100%', paddingTop: 2,
                                            paddingHorizontal: 16,
                                            backgroundColor: '#fff',
                                            flexDirection: 'row',
                                            marginTop: 1 * AppConstants.ActiveTheme.AppObjectSpacing
                                        }}>
                                            <Image
                                                style={{ width: 20, height: 20 }}
                                                source={require('../images/home2.png')}>
                                            </Image>
                                            <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 20, 
                                                    marginLeft: 1 * AppConstants.ActiveTheme.AppObjectSpacing  }}>
                                                {item.name_place}
                                            </Text>
                                        </View>
                                        <View style={{
                                            width: '100%', padding: 2,
                                            paddingHorizontal: 16,
                                            backgroundColor: '#fff',
                                            flexDirection: 'row'
                                        }}>
                                            <Image
                                                style={{ width: 20, height: 20 }}
                                                source={require('../images/placeIcon2.png')}>
                                            </Image>
                                            <Text style={{ color: '#000', marginLeft: 1 * AppConstants.ActiveTheme.AppObjectSpacing }}>
                                                {item.address}
                                            </Text>
                                        </View>
                                    </View>
                                </CardView>
                            </View>
                        </TouchableOpacity>} keyExtractor={(item, index) => item.id_place}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.refreshing}
                            onRefresh={this.props._onRefresh}
                        />
                    }
                />
            </View>
        );
    }
    
}