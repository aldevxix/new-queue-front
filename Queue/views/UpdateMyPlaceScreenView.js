import React, { Component } from 'react';
import { View, Dimensions, Text , Alert, Image, TouchableOpacity, ScrollView } from 'react-native';
import { GlobalStyles } from '../assets/GlobalStyles';
import { AppConstants } from "../systems/Constants";
import  ImageView  from "./components/ImageView";
import Button  from './components/Button';
import InputBottom from './components/InputBottom';
import { link } from '../systems/Config'
export default class UpdateMyPlaceScreenVIew extends Component {

    constructor(props) {

        super(props);

        this.state = {
            showDefault :  true
        };
    }

    render() {
        return <View style={[GlobalStyles.Wrapper, {}]}>
                    
                <View style={{ justifyContent: "center", alignItems: "center", borderBottomWidth: 2, borderBottomColor: '#EDEDED' }}>
                    <ImageView
                        imageSrc={require('../assets/images/myplace.png')}
                        width={Dimensions.get('window').width / 3}
                        height={7 * AppConstants.ActiveTheme.AppObjectSpacing} />
                </View>

                <ScrollView showsVerticalScrollIndicator = {false}>
                    
                    <View style={{flex : 1,alignItems :'center',marginVertical : 24}}>
                        <View   style={{borderWidth : 2, paddingTop : 2.5, paddingLeft : 2.5,
                                width : Dimensions.get("window").width - (48),
                                height : Dimensions.get("window").height/3,
                                borderColor:AppConstants.ActiveTheme.AppButtonColorBlueNavy}}>
                                
                            <Image  style={{ marginBottom : 20,
                                    width : Dimensions.get("window").width - (58),
                                    height : Dimensions.get("window").height/3.2}} 
                                    source={this.props.showDefault == false ? {uri : this.props.imageSource} :
                                            this.props.imageSource}
                                    >

                            </Image>
                        </View>

                        <TouchableOpacity onPress={()=> this.props.selectPhoto()}>
                            <View style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                width : Dimensions.get("window").width - (48),
                                height : 40,
                                backgroundColor: AppConstants.ActiveTheme.AppButtonColorBlueNavy,
                                marginBottom: 24}}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 18,
                                }}>Select Photo</Text>
                            </View>
                        </TouchableOpacity>
                                
                        <InputBottom
                            style={{borderRadius : 0 * AppConstants.ActiveTheme.AppObjectSpacing, borderTopWidth: 0, borderRightWidth: 0, borderLeftWidth: 0}}
                            placeholder={'Nama Tempat'} 
                            defaultValue = {this.props.defaultNamePlace}
                            onChangeText={this.props.NamePlace}
                            ImageSrc={require('../assets/images/iconn.png')}
                            width={Dimensions.get('window').width - (48)}/>

                        <InputBottom
                            style={{borderRadius : 0 * AppConstants.ActiveTheme.AppObjectSpacing, borderTopWidth: 0, borderRightWidth: 0, borderLeftWidth: 0}}
                            placeholder={'Alamat Tempat'} 
                            onChangeText={this.props.Alamat}
                            defaultValue={this.props.defaultAlamat}
                            ImageSrc={require('../assets/images/iconnn.png')}
                            width={Dimensions.get('window').width - (48)}/>

                        <View style={{marginTop: 20}}>
                            <Button
                                style={{borderRadius : 4 * AppConstants.ActiveTheme.AppObjectSpacing}}
                                width={Dimensions.get('window').width - (10 * AppConstants.ActiveTheme.AppObjectSpacing)}
                                height={AppConstants.ActiveTheme.AppInputHeightDefault + (1* AppConstants.ActiveTheme.AppObjectSpacing)}
                                label={'Save'}
                                onPress={this.props.sendData}
                                radius={AppConstants.ActiveTheme.AppObjectSpacing} />
                        </View>
                        
                    </View>
                </ScrollView>
            </View>
    }
}