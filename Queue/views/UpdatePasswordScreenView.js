import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity } from 'react-native';
import { GlobalStyles } from '../assets/GlobalStyles';
import { AppConstants } from "../systems/Constants";
import ImageView from './components/ImageView';
import Input from './components/Input';
import Button from './components/Button';

export default class UpdatePasswordScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        return <View style={[GlobalStyles.Wrapper]}>

            <View style={{flexDirection: "row", justifyContent:"center", alignItems:"center", borderBottomWidth: 2, borderBottomColor: '#EDEDED'}}>
                
                <TouchableOpacity style={{flex: 1}} onPress={()=>this.props.onPressBack()}>
                    <View style={{flexDirection: "row"}}>
                        <ImageView
                            imageSrc={require('../assets/images/back.png')}
                            width={Dimensions.get('window').width / 10}
                            height={3 * AppConstants.ActiveTheme.AppObjectSpacing}
                            />
                    </View>
                </TouchableOpacity>
                
                <View style={{flex: 1, alignItems: 'center'}}>
                    <ImageView
                        imageSrc={require('../assets/images/profil.png')}
                        width={Dimensions.get('window').width / 3}
                        height={7 * AppConstants.ActiveTheme.AppObjectSpacing}
                        />
                </View>
                
               <View style={{flex:1}}></View>

            </View>     

            <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 16}}>
               
                <Input
                    ImageSrc={require('../assets/images/lock.png')}
                    style={{borderRadius : 3 * AppConstants.ActiveTheme.AppObjectSpacing}}
                    placeholder={'Kata sandi baru'}
                    secureMode={true} 
                    onChangeText = {this.props.pass}/>
                <Input
                    ImageSrc={require('../assets/images/lock.png')}
                    style={{borderRadius : 3 * AppConstants.ActiveTheme.AppObjectSpacing}}
                    placeholder={'Ketik ulang kata sandi baru'}
                    secureMode={true} 
                    onChangeText = {this.props.confirmPass}/>    
                    
                <View style={{justifyContent : "flex-start", alignItems:"center"}}>
                    <Button
                        style={{borderRadius : 4 * AppConstants.ActiveTheme.AppObjectSpacing}}
                        width={Dimensions.get('window').width - (4 * AppConstants.ActiveTheme.AppObjectSpacing)}
                        height={AppConstants.ActiveTheme.AppInputHeightDefault + (1* AppConstants.ActiveTheme.AppObjectSpacing)}
                        label={'UBAH'}
                        onPress={()=>this.props.updatePass()}
                        radius={AppConstants.ActiveTheme.AppObjectSpacing} /> 
                </View>
            </View>
        </View>
    }
}
