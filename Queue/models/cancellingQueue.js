import {link} from '../systems/Config'

export var hapusData = null;
export function cancellingQueue (queue_code) {  
    return new Promise((resolve,reject)=>{
        fetch(link + 'place/cancelQueue',
        {
            method : 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
        },
            body: JSON.stringify({
                queue_code : queue_code
            })
      }).then((response)=> response.json())
      .then((responseJson)=>{
          
        hapusData = responseJson;
        resolve(true);
      }).catch((error) => {
              console.error(error);
              reject(true);
            });

    })  
}
