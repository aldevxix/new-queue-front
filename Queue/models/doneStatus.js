import {link} from '../systems/Config'

export var responseDone = null;
export function updateToDone (queue_code) {  
    return new Promise((resolve,reject)=>{
        fetch( link + 'place/updateQueueStatus',
        {
            method : 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
        },
            body: JSON.stringify({
                queue_code : queue_code
            })
      }).then((response)=> response.json())
      .then((responseJson)=>{
          
        responsData = responseJson;
        // this.setData(responseJson);
        resolve(true);
      }).catch((error) => {
              console.error(error);
              reject(true);
            });

    })  
                    //  return true;
}
