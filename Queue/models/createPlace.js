import {link} from '../systems/Config'

export var responseData = null

export function imageUpload(data) {
    return new Promise((resolve, reject) => {

        fetch( link + 'place/placeRegImage', {
            method: 'post',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'multipart/form-data'
            },
            body : data
        })
        .then((response) => response.json())
        .then((responseJson) => {

            responseData = responseJson
            resolve(true)

        }).catch((error) => {
            console.error(error);

            reject(true)
        });
    })
}
