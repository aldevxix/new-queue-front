import {link} from '../systems/Config'

export var responseData;

export function editProfile(formData) {
    return new Promise((resolve, reject) => {

        fetch( link + 'auth/updateProfil', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type' : 'multipart/form-data'
            },
            body : formData
        })
        .then((response) => response.json())
        .then((responseJson) => {

            responseData = responseJson
            resolve(true)

        }).catch((error) => {
            console.error(error);

            reject(true)
        });
    })
}