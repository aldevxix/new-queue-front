import {link} from '../systems/Config'

export var responsData = null;
export function searchPlace (name_place) {  
    return new Promise((resolve,reject)=>{
        fetch( link + 'place/search',
        {
            method : 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
        },
            body: JSON.stringify({
                name_place : name_place
            })
      }).then((response)=> response.json())
      .then((responseJson)=>{
          
        responsData = responseJson;
        resolve(true);
      }).catch((error) => {
              console.error(error);
              reject(true);
            });

    }) 
}
