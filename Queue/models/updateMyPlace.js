import {link} from '../systems/Config'

export var responseData;

export function updateMyPlace(formData) {
    return new Promise((resolve, reject) => {

        fetch( link + 'place/updatePlace', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type' : 'multipart/form-data'
            },
            body : formData
        })
        .then((response) => response.json())
        .then((responseJson) => {

            responseData = responseJson
            resolve(true)

        }).catch((error) => {
            console.error(error);

            reject(true)
        });
    })
}