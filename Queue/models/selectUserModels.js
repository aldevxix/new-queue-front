import {link} from '../systems/Config'

export var responseData;
export function selectUser (Email) {

    return new Promise((resolve, reject) => {
        fetch( link + 'auth/beforeForgetPass', {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: Email,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                responseData = responseJson;
                resolve(true);

            }).catch((error) => {
                console.error(error);
                reject(true);
            });
        })
}