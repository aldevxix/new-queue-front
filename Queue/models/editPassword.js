import {link} from '../systems/Config'

export var responseData;

export function editPass (id, Password){
    return new Promise((resolve, reject) => {
        fetch( link + 'auth/forgotPass', {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_user : id,
                password: Password,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                responseData = responseJson;
                resolve(true);

            }).catch((error) => {
                console.error(error);
                reject(true);
            });
    })
}