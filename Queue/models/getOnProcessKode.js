import {link} from '../systems/Config'

export var responseKode = null;
export function getOnProcessKode (id_place) {  
    return new Promise((resolve,reject)=>{
        fetch( link + '/place/getOnProcessKode',
        {
            method : 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
        },
            body: JSON.stringify({
                id_place : id_place
            })
      }).then((response)=> response.json())
      .then((responseJson)=>{
          
        responseKode = responseJson;
        // this.setData(responseJson);
        resolve(true);
      }).catch((error) => {
              console.error(error);
              reject(true);
            });

    })  
                    //  return true;
}
