import {link} from '../systems/Config'

export var responsData = null;
    export function dataPlace () {  
        return new Promise((resolve,reject)=>{
            fetch( link + 'place/openPlace',
            {
                method : 'GET',
                headers:{
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
            }
          }).then((response)=> response.json())
          .then((responseJson)=>{
              
            responsData = responseJson;
            resolve(true);
          }).catch((error) => {
                  console.error(error);
                  reject(true);
                });
    
        });  
    }
    