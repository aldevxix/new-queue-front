import {link} from '../systems/Config'

export var resetData = null;
export function resetDataQueue(id_place) {  
    return new Promise((resolve,reject)=>{
        fetch( link + 'place/resetQueue',
        {
            method : 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
        },
            body: JSON.stringify({
                id_place : id_place
            })
      }).then((response)=> response.json())
      .then((responseJson)=>{
          
        resetData = responseJson;
        resolve(true);
      }).catch((error) => {
              console.error(error);
              reject(true);
            });

    })  
}
