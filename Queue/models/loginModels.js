import {link} from '../systems/Config'

export var responsData = null;
export function loginFunc (email,password) {  
    return new Promise((resolve,reject)=>{
        fetch( link + 'auth/login',
        {
            method : 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
        },
            body: JSON.stringify({
             email : email,
             password : password
            })
      }).then((response)=> response.json())
      .then((responseJson)=>{
          
        responsData = responseJson;
        // this.setData(responseJson);
        resolve(true);
      }).catch((error) => {
              console.error(error);
              reject(true);
            });

    })  
                    //  return true;
}
