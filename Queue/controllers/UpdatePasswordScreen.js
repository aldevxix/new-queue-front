import UpdatePasswordScreenView from '../views/UpdatePasswordScreenView'
import React from 'react';
import { Alert, AsyncStorage, } from 'react-native';
import {editPass, responseData} from '../models/editPassword'

export default class UpdatePasswordScreen extends React.Component {
    constructor(props) {

        super(props);

        this.state = {
            id_user : "",
            password : "",
            confirmPassword : ""
        }

        this.content = [];
    }

    componentWillMount () {
        AsyncStorage.getItem('id_user',(error , result)=>{
            this.setState({
                id_user : result
            });
        })
    }

    updatePass = async () => {
        if(this.state.password != ""){
            if(this.state.confirmPassword == this.state.password){
                await editPass(this.state.id_user, this.state.password)
                .then(res => {
    
                    if(responseData.status){
                        Alert.alert("Success");
                        this.props.navigation.goBack(null)
                    } else {
                        Alert.alert("Failed");
                    }
                })
            }
            else{
                Alert.alert("Gagal !", "Pastikan Password anda sama");
            }
        }
        else{
            Alert.alert("Gagal !", "Kata Sandi tidak boleh kosong");
        }
        

        
        
    }

    back = async () => {
        this.props.navigation.goBack(null)
    }

    render = () => {
        return <UpdatePasswordScreenView 
            updatePass={()=> this.updatePass()}
            pass = {password => this.setState({password})}
            confirmPass = {confirmPassword => this.setState({confirmPassword})}
            onPressBack = {() => this.back()}
        />
    }
}