import EditProfileScreenView from '../views/EditProfileScreenView'

import React from 'react';
import { Keyboard, AsyncStorage, Alert } from 'react-native';
import { responseData, editProfile } from '../models/editProfile';
import {showImagePicker} from 'react-native-image-picker';

const options = {
    title: "Pilih Gambar",
    takePhotoButtonTitle: "Buka Kamera",
    chooseFromLibraryButtonTitle: "Pilih Dari Galeri",
    quality: 1
}

export default class EditProfileScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            toScroll: false,
            imageSource: null,
            dataName: null,
            dataType: null,
            dataUri: null,
            name: '',
            email: '',
            password: '',
            photoprofile: '',
        }

        this.content = [];
    }

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);

        AsyncStorage.getItem('name',(error , result)=>{
            this.setState({
                name : result
            });
        })
        AsyncStorage.getItem('email',(error , result)=>{
            this.setState({
                email : result
            });
        })
        AsyncStorage.getItem('id_user',(error , result)=>{
            this.setState({
                id_user : result
            });
        })
        AsyncStorage.getItem('photoprofile',(error , result)=>{
            this.setState({
                photoprofile : result
            });
        })
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    
    _keyboardDidShow = () => {
        this.setState({ toScroll: true });
    }
    
    _keyboardDidHide = () => {
        this.setState({ toScroll: false });
    }

    updateData () {
        AsyncStorage.setItem("email",this.state.email)
        AsyncStorage.setItem("name",this.state.name)
        AsyncStorage.setItem("photoprofile",this.state.photoprofile)
    }

    selectPhoto = () => {
        showImagePicker(options, (response) => {
            console.log("Response = ", response);

            if(response.didCancel)  Alert.alert("User cancelled image picker");
            else if(response.error) Alert.alert("ImagePicker Error: ", response.error);
            else {
                let source = { uri : response.uri }
                this.setState({
                    imageSource: source,
                    dataUri: response.uri,
                    dataType: response.type,
                    dataName: response.fileName
                })
                Alert.alert("", "Photo Selected")
            }
        })
    }

    save = async () => {

        const formData = new FormData();
        
        formData.append('id_user', this.state.id_user)
        formData.append('name', this.state.name);
        formData.append('email', this.state.email);

        formData.append('photoprofile', {
            uri: this.state.dataUri,
            name: this.state.dataName,
            type: this.state.dataType,
        }); 

        await editProfile(formData).then(res => {
            if(responseData.status == true){

                this.setState({
                    photoprofile : responseData.data
                })
                
                this.updateData();
                Alert.alert("Update Success");
                this.props.navigation.goBack(null);
            }
            else if(responseData.status == false) Alert.alert("Update gagal");
        
        })
    }

    back = async () => {
        this.props.navigation.goBack(null);
    }

    edit = async () => {
        this.props.navigation.push("Profil")
    }

    render = () => {
        const { toScroll } = this.state;
        return <EditProfileScreenView onPressBack={()=> this.back()}
        selectPhoto={()=> this.selectPhoto()}
        onPressSave={()=> this.save()}
        scrollEnabled={toScroll} 
        onPressEdit={()=> this.edit()}
        Name={name => this.setState({name})}
        Email={email => this.setState({email})}
        photoprofile = {this.state.photoprofile}
        defaultName = {this.state.name}
        defaultEmail = {this.state.email}/>
    }
}