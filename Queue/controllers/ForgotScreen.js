import React from 'react';
import { BackHandler, Alert } from 'react-native'
import ForgotPassScreen from '../views/ForgotScreenView'
import {selectUser, responseData} from '../models/selectUserModels'

export default class ForgotScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Email : "",
        }

        this.content = [];
    }

    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handlerBack);
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handlerBack);
    }

    _onBackPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handlerBack)
    }

    handlerBack = () => {
        this.props.navigation.goBack(null);
        return true
    }

    disableButton = () => {

    }

    forgotCode = async () => {
        await selectUser(this.state.Email)
        .then(() => {

            if(responseData.status){
                var data = responseData.data.id_user; 

                Alert.alert("User Found", "ID: " +data)
                this.props.navigation.push("NewPassword", {user: ""+data});
            } else {
                Alert.alert("User Not Found");
            }
        })
    }

    render = () => {
        return <ForgotPassScreen onPressCode={() => this.forgotCode()}
        email = {Email => this.setState({Email})}/>
    }
}