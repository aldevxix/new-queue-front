import React from 'react';
import {Alert, AsyncStorage} from "react-native";
import UpdateMyPlaceScreenView from "../views/UpdateMyPlaceScreenView";
import { showImagePicker } from 'react-native-image-picker';
import { responseData, updateMyPlace } from "../models/updateMyPlace";
// import { responseRegPlace,registerPlace } from "../models/regPlace";
const options = {
    title: "Pilih Gambar",
    takePhotoButtonTitle: "Buka Kamera",
    chooseFromLibraryButtonTitle: "Pilih Dari Galeri",
    quality: 1
}

export default class UpdateMyPlaceScreen extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            showDefault : false,
            id_place: this.props.navigation.state.params.id_place,
            imageSource: this.props.navigation.state.params.picture,
            dataUri: null,
            dataType: null,
            dataName: null,
            namaTempat : this.props.navigation.state.params.name_place,
            alamat: this.props.navigation.state.params.address,
            picture : this.props.navigation.state.params.picture,
            inisial : '',
        }

        this.content = [];
    }
    componentWillMount = () =>{
        AsyncStorage.getItem('id_user',(error , result)=>{
            this.setState({
                id_user : result
            });
        })
        // this.Alert()
    }
    Alert = async () => {
        Alert.alert(this.props.navigation.state.params.picture);
    }
    selectPhoto = () => {
        showImagePicker(options, (response) => {
            console.log("Response = ", response);

            if(response.didCancel)  Alert.alert("User cancelled image picker");
            else if(response.error) Alert.alert("ImagePicker Error: ", response.error);
            else {
                let source = { uri : response.uri }

                this.setState({
                    imageSource: source,
                    dataUri: response.uri,
                    dataType: response.type,
                    dataName: response.fileName,
                    showDefault : true
                })
            }
        })
    }
    update = async () => {

        const data = new FormData();
        
        data.append('id_place', this.state.id_place);
        data.append('name_place', this.state.namaTempat);
        data.append('address', this.state.alamat);

        data.append('picture', {
            uri: this.state.dataUri,
            name: this.state.dataName,
            type: this.state.dataType,
        }); 

        await updateMyPlace(data).then(res => {
            if(responseData.status == true){ 
                Alert.alert("Update Sukses")
                this.props.navigation.goBack(null)
            }
            else if(responseData.status == false) Alert.alert("Update Gagal");
        
        })

    }
    render = () => {
        return <UpdateMyPlaceScreenView
        selectPhoto = {()=> this.selectPhoto()}
        sendData = {()=> this.update()}
        Alert = {() => this.Alert()}
        imageSource = {this.state.imageSource}
        defaultAlamat={this.state.alamat}
        picture = {this.state.picture}
        showDefault = {this.state.showDefault}
        defaultNamePlace = {this.state.namaTempat}
        NamePlace = {namaTempat => this.setState({namaTempat})}
        Alamat = {alamat => this.setState({alamat})}
        Inisial = {inisial => this.setState({inisial})} />
    }


}