import React from 'react';
import ForgotCode from '../views/ForgotCodeView'

export default class ForgotCodeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // user : this.props.navigation.state.params.id_user
        }
        this.content = [];
    }
    disableButton = () => {

    }

    newPass = async () => {
        this.props.navigation.push("NewPassword");
    }

    render = () => {
        return <ForgotCode onPressNew={()=> this.newPass()}/>
    }
}