import NewPasswordScreenView from '../views/NewPasswordScreenView'
import React from 'react';
import { Alert, Keyboard } from 'react-native';
import {forgotHandler, responseData} from '../models/forgotPassModels'

export default class NewPasswordScreen extends React.Component {
    constructor(props) {

        super(props);

        this.state = {
            id : this.props.navigation.state.params.user,
            password : "",
            confirmPassword : ""
        }

        this.content = [];
    }

    New = async () => {

        if(this.state.password != ""){
            if(this.state.confirmPassword == this.state.password){
                await forgotHandler(this.state.id, this.state.password)
                .then(() => {
    
                    if(responseData.status){
                        Alert.alert("Success");
                        this.props.navigation.push("LoginView")
                    } else {
                        Alert.alert("Failed");
                    }
                })
            }
            else{
                Alert.alert("Gagal !", "Pastikan Password anda sama");
            }
        }
        else{
            Alert.alert("Gagal !", "Kata Sandi tidak boleh kosong");
        }
        
        
    }

    render = () => {
        return <NewPasswordScreenView onPressNewPass={()=> this.New()}
            newPass = {password => this.setState({password})}
            confirmNewPass = {confirmPassword => this.setState({confirmPassword}) }
            />
    }
}